x2gobroker (0.0.4.3-4) unstable; urgency=medium

  * debian/*.postrm:
    + De-duplicate delgroup calls. Thanks to Patrice Duroux for highlighting
      this. (Releates to #1035847).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 19 May 2023 10:54:13 +0200

x2gobroker (0.0.4.3-3) unstable; urgency=medium

  [ Johannes Schauer Marin Rodrigues ]
  * debian/*.postrm:
    + Ignore failures when evoking (possibly not installed) deluser. (Closes:
      #1035847).

  [ Mike Gabriel ]
  * debian/po:
    + Add Spanish debconf translation. Thanks to Camaleón. (Closes: #987522).
    + Add Brazilian Portuguese debconf translation. Thanks to Paulo Henrique de
      Lima Santana. (Closes: #1026413).
  * debian/control:
    + Drop from D: lsb-base (obsoleted package). Thanks, lintian.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 16 May 2023 21:27:13 +0200

x2gobroker (0.0.4.3-2) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.
  * debian/patches:
    + Add 2001_no_2to3_supported_any_more.patch. Don't run setup.py with
      use_2to3 anymore. Support for it has been dropped. (Closes: #997604).
    + Forward patches 1003 and 2001.
    + Add 0001_man-man1-x2gobroker.1-Fix-apostrophe-usage-drop-prec.patch.
      Thanks, lintian.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 15 Nov 2021 15:17:01 +0100

x2gobroker (0.0.4.3-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add Rules-Requires-Root: field and set it to no.
    + Bump Standards-Version: to 4.5.0. No changes needed.
    + Add D (x2gobroker-ssh): procps.
  * debian/patches:
    + Add 1003_handle-offline-builds.patch. Enable offline builds.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/x2gobroker-ssh.postinst:
    + Only invalidate nscd group cache if nscd is running / in use. (See X2Go
      upstream bug #1458).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 16 Nov 2020 10:30:52 +0100

x2gobroker (0.0.4.1-1) unstable; urgency=medium

  * New upstream release (with specific fixes, targetting Debian buster).
    - Make builds reproducible. Thanks to Chris Lamb for providing a patch.
      (Closes: #922137).
    - Correctly initialize the loggers when using x2gobroker via WSGI in
      Apache2. (Closes: #922042).
    - x2gobroker-wsgi: Place WSGI script symlink (pointing to
      <BINDIR>/x2gobroker) into a dedicated folder and configure Apache2
      to use WSGIScriptAlias from that folder. (Closes: #922040).
    - Permit `asyncio` to create event loops on any thread (required on
      multithreaded WSGI servers using Python 3). (Closes: #925102).
    - Make remote agent's SSH HostKey policy configurable globally, backend-wise
      and per session profile. Fallback to RejectPolicy by default.
      (Closes: #922314).
    - x2gobroker/brokers/base_broker.py: Log IP address of authentication
      attempts. (Closes: #922458).
  * debian/patches:
    + Drop all patches. All changes applied upstream.
  * debian/changelog:
    + Fix indentation in previous changelog stanza.
  * debian/po:
    + Add French debconf translation. Thanks to Jean-Pierre Giraud. (Closes:
      #923381).
    + Add Dutch debconf translation. Thanks to Frans Spiesschaert. (Closes:
      #922774).
    + Add Portuguese debconf translation file. Thanks to Américo Monteiro.
      (Closes: #922527).
  * debian/x2gobroker-wsgi.install:
    + Add WSGIScriptAlias symlink path.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 23 Apr 2019 11:24:49 +0200

x2gobroker (0.0.4.0-3) unstable; urgency=medium

  * debian/patches:
    + Add 0001_sbin-x2gobroker-keygen-Fix-call-to-x2gobroker.utils..patch.
      Fix call to x2gobroker.utils.get_key_fingerprint_with_colons(). Thanks
      to Linnea Skogtvedt for spotting and reporting this.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 06 Feb 2019 19:41:33 +0100

x2gobroker (0.0.4.0-2) unstable; urgency=medium

  * debian/control:
    + Fix Vcs-*: fields. Copy+paste flaw (x2goclient -> x2gobroker).
  * debian/rules:
    + Only evoke doc cleanup, if Makefile.docupload is present. This fixes
      src:package builds in debian/-folder-only packaging repos.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 Feb 2019 08:55:02 +0100

x2gobroker (0.0.4.0-1) unstable; urgency=medium

  * Initial release to Debian. (Closes: #906073).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 02 Feb 2019 11:44:34 +0100
