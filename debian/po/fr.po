# Translation of x2gobroker debconf templates to French
# Copyright (C) 2019 Debian French l10n Team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the x2gobroker package.
#
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: x2gobroker\n"
"Report-Msgid-Bugs-To: x2gobroker@packages.debian.org\n"
"POT-Creation-Date: 2019-02-03 11:44+0100\n"
"PO-Revision-Date: 2019-02-14 11:27+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:2001
msgid "Create group for X2Go Broker SSH access now?"
msgstr "Faut-il créer maintenant un groupe pour l'accès SSH au courtier X2Go ?"

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:2001
msgid ""
"In X2Go Session Broker, SSH-based broker access is controlled via the broker "
"users' membership in a dedicated group (default: x2gobroker-users)."
msgstr ""
"Avec le courtier de session X2Go, l'accès au courtier basé sur SSH est "
"contrôlé grâce à l'appartenance des utilisateurs du courtier à un groupe "
"dédié (par défaut : x2gobroker-users)."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:2001
msgid ""
"If this group is not created now, you will be asked to assign this privilege "
"to an existing group instead."
msgstr ""
"Si ce groupe n'est pas déjà créé, il vous sera demandé d'assigner à la place "
"ces droits à un groupe déjà existant."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:3001
msgid "Use an already existing group for X2Go Session Broker SSH access?"
msgstr ""
"Faut-il utiliser un groupe déjà existant pour l'accès SSH au courtier de "
"session X2Go ?"

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:3001
msgid ""
"If there already exists a group (e.g. in an LDAP database) that you would "
"like to use for controlling X2Go Session Broker SSH access with, then you "
"can specify this group name with the next step."
msgstr ""
"S'il existe déjà un groupe, (par exemple dans une base de données LDAP) que "
"vous souhaiteriez utiliser pour contrôler l'accès SSH au courtier de session "
"X2Go, vous pouvez spécifier ce nom de groupe dans l'étape suivante."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:4001
msgid "Set up X2Go Session Broker SSH access later?"
msgstr ""
"Voulez-vous configurer plus tard l'accès SSH au courtier de session X2Go ?"

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:4001
msgid ""
"Without an existing group for X2Go Session Broker SSH access, the SSH broker "
"will not be usable by users. You have to set up things later, either "
"manually or via this configuration helper."
msgstr ""
"S'il n'existe pas de groupe pour l'accès SSH au courtier de session X2Go, le "
"courtier SSH ne sera pas accessible aux utilisateurs. Vous devrez configurer "
"cela plus tard soit manuellement, soit avec cet assistant de configuration."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:4001
msgid ""
"A manual setup is only recommended, if you really know what have to do for "
"this."
msgstr ""
"La configuration manuelle n'est recommandée que si vous savez vraiment ce "
"qu'il faut faire pour la mener à bien."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:4001
msgid "Alternatively, the setup questions can be asked once more..."
msgstr "Sinon, les questions de configuration peuvent être à nouveau "
"posées..."

#. Type: string
#. Description
#: ../x2gobroker-ssh.templates:5001
msgid "X2Go Session Broker SSH access group:"
msgstr "Groupe d'accès SSH au courtier de session X2Go :"

#. Type: string
#. Description
#: ../x2gobroker-ssh.templates:5001
msgid ""
"Please specify the group name for users with full X2Go Session Broker access "
"via SSH now."
msgstr ""
"Veuillez spécifier maintenant le nom du groupe pour les utilisateurs dotés "
"d'un droit d'accès au courtier de session X2Go à travers SSH."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:6001
msgid ""
"Delete the group that was formerly used for X2Go Session Broker SSH access?"
msgstr "Faut-il supprimer le groupe précédemment utilisé pour l'accès SSH au "
"courtier de session X2Go ?"

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:6001
msgid "The group for X2Go Session Broker SSH access has been modified."
msgstr "Le groupe d'accès SSH au courtier de session X2Go a été modifié."

#. Type: boolean
#. Description
#: ../x2gobroker-ssh.templates:6001
msgid ""
"Please specify whether the old group should be deleted from your system. If "
"unsure, keep the formerly used group and manually investigate later."
msgstr ""
"Veuillez préciser si l'ancien groupe doit être supprimé sur la machine. Si "
"vous n'êtes pas sûr, conservez le groupe anciennement utilisé et poursuivez "
"votre recherche plus tard."

#. Type: note
#. Description
#: ../x2gobroker-ssh.templates:7001
msgid "The specified group does not exist on the system"
msgstr "Le groupe spécifié n'existe pas sur la machine."

#. Type: note
#. Description
#: ../x2gobroker-ssh.templates:7001
msgid "Please enter a group name that is currently available on your system."
msgstr ""
"Veuillez entrer un nom de groupe existant réellement sur votre machine."

#. Type: note
#. Description
#: ../x2gobroker-ssh.templates:7001
msgid "Please, try again!"
msgstr "Veuillez essayer à nouveau !"
